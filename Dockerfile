FROM gradle:8.3.0-jdk17-alpine AS build-stage
WORKDIR /app
COPY . .
RUN gradle bootJar

FROM openjdk:17-oraclelinux8
WORKDIR /deploy
COPY --from=build-stage /app/build/libs/products.managment-1.0.0.jar .
ENTRYPOINT ["java", "-jar", "products.managment-1.0.0.jar"]