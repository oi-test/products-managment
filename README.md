# Spring Boot Products Management App
Kotlin-based Spring Boot application for managing products

## Prerequisites
Before you begin, ensure you have the following prerequisites installed on your machine:
- [Docker](https://www.docker.com/get-started)
- [Docker Compose](https://docs.docker.com/compose/install/)
- Java Development Kit (JDK) 17 or later
- Gradle or you can use Gradle Wrapper


## Getting Started

### Clone the Repository
```
git clone git@gitlab.com:oi-test/products-managment.git
```

### Run with Docker Compose
You can run the application and database with Docker compose using one command:
```
docker-compose up
```

### Accessing the Application
Once the containers are up and running, you can access the application with Swagger UI in your web browser:
[http://localhost:8080/swagger-ui/index.html](http://localhost:8080/swagger-ui/index.html)

### Running Unit Tests
You can run the unit tests using the following command:
```
./gradlew test
```
