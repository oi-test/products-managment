package com.imedia24.products.managment.controller;

import com.imedia24.products.managment.dto.AddProductDto
import com.imedia24.products.managment.dto.ProductDto
import com.imedia24.products.managment.dto.UpdateProductDto
import com.imedia24.products.managment.service.ProductService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PatchMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/products")
class ProductController(@Autowired private val productService: ProductService) {

    @GetMapping("/{sku}")
    fun getProduct(@PathVariable sku: Long): ProductDto {
        return productService.getProduct(sku)
    }

    @GetMapping
    fun getProducts(@RequestParam skus: List<Long>): List<ProductDto> {
        return productService.getProducts(skus)
    }

    @PatchMapping("/{sku}")
    fun updateProduct(@PathVariable sku: Long, @RequestBody product: UpdateProductDto) {
        productService.updateProduct(sku, product)
    }

    @PostMapping
    fun addProduct(@RequestBody product: AddProductDto) {
        productService.addProduct(product)
    }

    @DeleteMapping("/{sku}")
    fun removeProduct(@PathVariable sku: Long) {
        productService.deleteProduct(sku)
    }
}