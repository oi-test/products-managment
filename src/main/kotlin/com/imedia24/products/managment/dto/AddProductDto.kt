package com.imedia24.products.managment.dto

data class AddProductDto(
    val name: String,
    val price: Double,
    val stock: Int
)