package com.imedia24.products.managment.dto

data class ProductDto(
    val sku: Long,
    val name: String,
    val price: Double,
    val stock: Int
)