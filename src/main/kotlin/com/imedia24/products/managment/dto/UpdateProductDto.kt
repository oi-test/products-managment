package com.imedia24.products.managment.dto
data class UpdateProductDto(
    val name: String?,
    val price: Double?,
    val stock: Int?
)