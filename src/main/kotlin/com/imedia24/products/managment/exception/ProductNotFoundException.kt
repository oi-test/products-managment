package com.imedia24.products.managment.exception

class ProductNotFoundException(message: String?): RuntimeException(message) {
    constructor(): this(null)
}