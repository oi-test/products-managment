package com.imedia24.products.managment.exception.handler

import com.imedia24.products.managment.exception.ProductNotFoundException
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler

@ControllerAdvice
class ProductNotFoundHandler {
    @ExceptionHandler(ProductNotFoundException::class)
    fun handleCustomException(exception: ProductNotFoundException): ResponseEntity<String> {
        val errorMessage = "Product Not Found: ${exception.message}"
        return ResponseEntity(errorMessage, HttpStatus.NOT_FOUND)
    }
}