package com.imedia24.products.managment.mapper

import com.imedia24.products.managment.dto.AddProductDto
import com.imedia24.products.managment.dto.ProductDto
import com.imedia24.products.managment.dto.UpdateProductDto
import com.imedia24.products.managment.model.Product

class ProductMapper {
    companion object{
        fun toProductDto(product: Product): ProductDto{
            return ProductDto(product.sku!!, product.name, product.price, product.stock)
        }

        fun toProductIgnoreNulls(updateProductDto: UpdateProductDto, product: Product){
            product.name = updateProductDto.name ?: product.name
            product.price = updateProductDto.price ?: product.price
            product.stock = updateProductDto.stock ?: product.stock
        }

        fun toProduct(addProductDto: AddProductDto): Product{
            return Product(addProductDto.name, addProductDto.price, addProductDto.stock)
        }
    }
}