package com.imedia24.products.managment.model

import jakarta.persistence.Entity
import jakarta.persistence.GeneratedValue
import jakarta.persistence.GenerationType
import jakarta.persistence.Id
import jakarta.persistence.Table
import org.springframework.data.annotation.Version

@Entity
@Table(name="products")
data class Product(
    var name: String,
    var price: Double,
    var stock: Int
){
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val sku: Long? = null;

    @Version
    val version: Int = 1;
}