package com.imedia24.products.managment.repository

import com.imedia24.products.managment.model.Product
import org.springframework.data.repository.CrudRepository

interface ProductRepository: CrudRepository<Product, Long>