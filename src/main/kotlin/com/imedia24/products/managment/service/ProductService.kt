package com.imedia24.products.managment.service

import com.imedia24.products.managment.dto.AddProductDto
import com.imedia24.products.managment.dto.ProductDto
import com.imedia24.products.managment.dto.UpdateProductDto

interface ProductService {
    fun getProducts(skus: List<Long>): List<ProductDto>
    fun getProduct(sku: Long): ProductDto
    fun updateProduct(sku: Long, productDto: UpdateProductDto)
    fun addProduct(productDto: AddProductDto)
    fun deleteProduct(sku: Long)
}