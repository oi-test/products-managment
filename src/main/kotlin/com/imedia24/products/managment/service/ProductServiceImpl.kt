package com.imedia24.products.managment.service;

import com.imedia24.products.managment.dto.AddProductDto
import com.imedia24.products.managment.dto.ProductDto
import com.imedia24.products.managment.dto.UpdateProductDto
import com.imedia24.products.managment.mapper.ProductMapper
import com.imedia24.products.managment.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import com.imedia24.products.managment.exception.ProductNotFoundException

@Service
class ProductServiceImpl(@Autowired private val productRepository :ProductRepository): ProductService {
    override fun getProducts(skus: List<Long>): List<ProductDto> {
        return productRepository.findAllById(skus)
            .map { product->ProductMapper.toProductDto(product) }
    }

    override fun getProduct(sku: Long): ProductDto {
        val product = productRepository.findById(sku)
            .orElseThrow{
                ProductNotFoundException("Could not found product with sku=${sku}")
            }
        return ProductMapper.toProductDto(product)
    }

    override fun updateProduct(sku: Long, productDto: UpdateProductDto) {
        val product = productRepository.findById(sku)
            .orElseThrow{
                ProductNotFoundException("Could not found product with sku=${sku}")
            }
        ProductMapper.toProductIgnoreNulls(productDto, product)
        productRepository.save(product)
    }

    override fun addProduct(productDto: AddProductDto) {
        val product = ProductMapper.toProduct(productDto)
        productRepository.save(product)
    }

    override fun deleteProduct(sku: Long) {
        productRepository.findById(sku)
            .orElseThrow{
                ProductNotFoundException("Could not found product with sku=${sku}")
            }
        productRepository.deleteById(sku)
    }
}
