CREATE TABLE IF NOT EXISTS products (
    sku INT NOT NULL AUTO_INCREMENT,
    name VARCHAR(255) NOT NULL,
    price DECIMAL(10, 2) NOT NULL,
    version INT,
    stock INT,
    PRIMARY KEY (sku)
);

INSERT INTO products (sku, name, price, version, stock) 
VALUES 
    (1, 'Coca Cola', 5.5, 1, 1000),
    (2, 'Pringles', 10.5, 1, 1000),
    (3, 'Cheetos', 8.5, 1, 1000),
    (4, 'Cookies', 15, 1, 1000);