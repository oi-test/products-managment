package com.imedia24.products.managment.controller

import com.fasterxml.jackson.databind.ObjectMapper
import com.imedia24.products.managment.dto.AddProductDto
import com.imedia24.products.managment.dto.ProductDto
import com.imedia24.products.managment.dto.UpdateProductDto
import com.imedia24.products.managment.exception.ProductNotFoundException
import com.imedia24.products.managment.service.ProductService
import org.junit.jupiter.api.Test
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.*
import org.junit.jupiter.api.Assertions.assertEquals
import org.mockito.Mockito.`when`
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.http.HttpStatus

@WebMvcTest
class ProductControllerTest{

    @Autowired
    private lateinit var mockMvc: MockMvc

    @Autowired
    private lateinit var objectMapper: ObjectMapper

    @MockBean
    private lateinit var productService: ProductService

    @Test
    fun testGetProduct() {
        val sku = 123L
        val productDto = ProductDto(sku, "Product Name", 5.0, 1)

        `when`(productService.getProduct(sku)).thenReturn(productDto)

        val result = mockMvc.perform(get("/products/$sku"))
            .andExpect(status().isOk)
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andReturn()

        val response = objectMapper.readValue(result.response.contentAsString, ProductDto::class.java)

        assertEquals(HttpStatus.OK.value(), result.response.status)
        assertEquals(productDto.sku, response.sku)
        assertEquals(productDto.name, response.name)
        assertEquals(productDto.price, response.price)
    }

    @Test
    fun testGetProductNotFound() {
        val sku = 123L
        `when`(productService.getProduct(sku)).thenThrow(ProductNotFoundException())

        mockMvc.perform(get("/products/$sku"))
            .andExpect(status().isNotFound)
    }

    @Test
    fun testGetProducts() {
        val skus = listOf(123L, 456L)
        val productDto1 = ProductDto(123L, "Product 1", 5.0, 100)
        val productDto2 = ProductDto(456L, "Product 2", 5.0, 100)

        `when`(productService.getProducts(skus)).thenReturn(listOf(productDto1, productDto2))

        val result = mockMvc.perform(get("/products")
            .param("skus", skus.joinToString(",")))
            .andExpect(status().isOk)
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andReturn()

        val responseList = objectMapper.readValue(result.response.contentAsString, List::class.java)
        val response1 = responseList[0] as Map<*, *>
        val response2 = responseList[1] as Map<*, *>

        assertEquals(HttpStatus.OK.value(), result.response.status)

        println("SKU: "+productDto1.sku)
        println("RESPONSE SKU: "+response1["sku"])

        assertEquals(productDto1.sku, (response1["sku"] as Number).toLong())
        assertEquals(productDto1.name, response1["name"])
        assertEquals(productDto1.price, response1["price"])

        assertEquals(productDto2.sku, (response2["sku"] as Number).toLong())
        assertEquals(productDto2.name, response2["name"])
        assertEquals(productDto2.price, response2["price"])
    }

    @Test
    fun testUpdateProduct() {
        val sku = 123L
        val updateProductDto = UpdateProductDto("Updated Product Name", 10.0, 100)

        val result = mockMvc.perform(patch("/products/$sku")
            .contentType(MediaType.APPLICATION_JSON)
            .content(objectMapper.writeValueAsString(updateProductDto)))
            .andExpect(status().isOk)
            .andReturn()

        assertEquals(HttpStatus.OK.value(), result.response.status)
    }

    @Test
    fun testUpdateProductNotFound() {
        val sku = 123L
        val updateProductDto = UpdateProductDto("Updated Product Name", 10.0, 100)

        `when`(productService.updateProduct(sku, updateProductDto)).thenThrow(ProductNotFoundException())

        mockMvc.perform(patch("/products/$sku")
            .contentType(MediaType.APPLICATION_JSON)
            .content(objectMapper.writeValueAsString(updateProductDto)))
            .andExpect(status().isNotFound)
            .andReturn()
    }

    @Test
    fun testAddProduct() {
        val addProductDto = AddProductDto("New Product", 12.0, 100)

        val result = mockMvc.perform(post("/products")
            .contentType(MediaType.APPLICATION_JSON)
            .content(objectMapper.writeValueAsString(addProductDto)))
            .andExpect(status().isOk)
            .andReturn()

        assertEquals(HttpStatus.OK.value(), result.response.status)
    }

    @Test
    fun testRemoveProduct() {
        val sku = 123L

        val result = mockMvc.perform(delete("/products/$sku"))
            .andExpect(status().isOk)
            .andReturn()

        assertEquals(HttpStatus.OK.value(), result.response.status)
    }

    @Test
    fun testRemoveProductNotFound() {
        val sku = 123L

        `when`(productService.deleteProduct(sku)).thenThrow(ProductNotFoundException())

        mockMvc.perform(delete("/products/$sku"))
            .andExpect(status().isNotFound)
    }
}
